The SwissGenVar Platform aims at providing a national, joint, and curated database for constitutional variants coming from Swiss medical genetics centres.

# The project 
The SwissGenVar project was launched in 2019, with the support of the Swiss Personalized Health Network (SPHN, www.sphn.ch) initiative.

# Why SwissGenVar?
SwissGenVar aims to provide a high-quality and efficient platform for harmonization and up-scaling of expert germline variant interpretation by clinical genetic laboratories in Switzerland. This platform assures the methodological and technical prerequisites for national data sharing and international data interoperability and allows harvesting of consented data generated during routine health care for research into personalized medicine. 
SwissGenVar not only fosters harmonization and inclusion of diagnostic data, but also data generated within research projects using genomic sequencing approaches. This project thus provides a platform (currently accessible to the project partners only) for:
- **Nation-wide collection of genetic variants** identified in patients by Swiss clinical genetic laboratories in an consented interoperable data set with at least a minimal set of non-identifying clinical data using the internationally standardized Human Phenotype Ontology (HPO). 
- S**haring of evidence and variant interpretation** by clinical genetic experts.
- A**ccessibility of these expert-annotated variants for clinicians** and researchers, through an efficient, scalable, intuitive, and user-friendly platform, integrated into the BioMedIT landscape of SPHN (Swiss Personalized Health Network).

# SwissGenVar workflow
The principle of the project is that the medical genetics groups in Switzerland feed the system with the variants they identify in their patients when using NGS for diagnosis. The system then automatically gathers publicly available information from selected data sources, and the clinical expert can add their additional interpretation and findings to complete the genotype-phenotype description.
SwissGenVar to support also translational research
SwissGenVar incorporates variant information from other medical genetics-related platforms such as ClinVar or GnomAD, to facilitate the interpretation of variants by medical geneticists, to become the Swiss one-stop platform for the understanding of constitutional variants.
The annotation of variants will also be accessible to researchers, as having access to variants detected in patients with well-characterized and clinically-validated phenotypic information is critical to better understand disease aetiology, improve patient care and identify new drug targets.
So far, the platform is available to partner groups only. A public instance presenting variants-related information is planned for 2022.

# The partners
This program has been made possible thanks to the commitment and strong support of Swiss medical genetics centers and institutes who have agreed to share their know-how and expertise for the benefit of the patients.

**Institute of Medical Genetics, University of Zürich**

**Institute of Medical Molecular Genetics, University of Zürich**

**University Hospital of Basel**

**University Hospital of Bern**

**University Hospital of Geneva**

**University Hospital of Lausanne**

**Swiss Institute of Bioinformatics**


# About the team
Anita Rauch (University of Zurich) is Professor and director of the University’s Institute of Medical Genetics, with a research focus on developmental disorders, genotype-phenotype correlation, and breast and ovarian cancer susceptibility.

Marc Abramowicz (University Hospital of Geneva/University of Geneva) is Professor and Head of the Service de Médecine Génétique, with a research focus on molecular endocrinology, brain disorders and personalized, genomic medicine.

Wolfgang Berger (University of Zurich) is Professor and director of the University’s Institute of Medical Molecular Genetics, with a research focus on eye and heart disease (cardiomyopathies, long QT syndrome)

Sven Cichon (University Hospital of Basel, University of Basel) is Professor and chairman of the Division of Medical Genetics, with a research focus on genetically complex, multifactorial phenotypes, in particular neuropsychiatric disorders.

André Schaller (University Hospital of Bern, University of Bern) is Privatdozent and divisional head of Human Genetics in the Clinical Genomics Laboratory, with a research focus on mitochondrial and cardiovascular disorders.

Andrea Superti-Furga (University Hospital of Lausanne, University of Lausanne) is Professor and Head of the Division of Genetic Medicine, with a research focus on genetic disorders of bone, connective tissue, metabolism, morphogenesis and dysmorphology.

Christiane Zweier (University Hospital of Bern, University of Bern), head of Human Genetics with a research focus on neurodevelopmental disorders.

Valérie Barbié is Director of Clinical Bioinformatics at SIB Swiss Institute of Bioinformatics.

# Help

The platform documentation can be found at: [SwissGenVar documentation](https://pages.sib.swiss/project/swissgenvar-doc/)
