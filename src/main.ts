import Vue from 'vue'
import './plugins/vue-meta'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import './plugins/jquery'
import './plugins/bootstrap-toc'
import './plugins/bootstrap-toc.css'
import App from './App.vue'
import config from './config.yml'

Vue.config.productionTip = false
Vue.prototype.$config = config

new Vue({
  render: h => h(App),
}).$mount('#app')
