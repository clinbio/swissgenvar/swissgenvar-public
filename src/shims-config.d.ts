declare module 'vue/types/vue' {
    export interface Vue {
        $config: {
            baseurl: string,
            title: string
            custom_css: null|string
            logo: string,
            project: {
                name: string,
                tagline: string
            },
            params: {
                toc: boolean,
                footer: string
            }
        },
    }
}
