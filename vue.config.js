const Vue = require("vue");
module.exports = {
    chainWebpack: config => {
        config.module.rule('md')
            .test(/\.md/)
            .use('vue-loader')
            .loader('vue-loader')
            .end()
            .use('vue-markdown-loader')
            .loader('vue-markdown-loader/lib/markdown-compiler')
            .options({
                raw: true
            })
    },
    pages: {
        index: {
            entry: 'src/main',
            // the source template
            template: 'template/index.html',

        }
    },
    outputDir: 'public',
    filenameHashing: false,
    publicPath: 'https://pages.sib.swiss/project/swissgenvar/'
}
